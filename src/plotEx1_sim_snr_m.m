% close all
% clear all
% clc

N = 1000;

% e1 = [0.09694850 0.10265411 0.11112533 0.11968232];
% e1_me = [0.01281936 0.01532373 0.01753374 0.01681718]/sqrt(N);
% s1 = [6.08418555 5.59775366 4.98788282 4.39157436];
% 
% e2 = [0.03550348 0.03696014 0.03929313];
% e2_me = [0.00460329 0.00546938 0.00531563]/sqrt(N);
% s2 = [13.64203669 13.32893851 12.93209541];
% 
% e3 = [0.02246016 0.02332781];
% e3_me = [0.00294191 0.00316033]/sqrt(N);
% s3 = [17.31498623 17.02849300];
% 
% e4 = [0.01679707];
% e4_me = [0.00209523]/sqrt(N);
% s4 = [19.72989611];

s1 = mean(rsnr_sim_1(:,1:4));
s2 = mean(rsnr_sim_1(:,5:7));
s3 = mean(rsnr_sim_1(:,8:9));
s4 = mean(rsnr_sim_1(:,10));

% figure
hold on
plot(s1,'b-.','LineWidth',2)
h5 = plot(s1,'bo','MarkerSize',8,'LineWidth',2);

% for i = 1:length(e1)
%     me = 1.645*e1_me(i);
%     plot(i*[1 1],[e1(i)-me e1(i)+me],'b-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e1(i)+me*[1 1],'b-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e1(i)-me*[1 1],'b-','LineWidth',1)
% end

plot(s2,'r-.','LineWidth',2)
h6 = plot(s2,'rv','MarkerSize',8,'LineWidth',2);

% for i = 1:length(e2)
%     me = 1.645*e2_me(i);
%     plot(i*[1 1],[e2(i)-me e2(i)+me],'r-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e2(i)+me*[1 1],'r-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e2(i)-me*[1 1],'r-','LineWidth',1)
% end

plot(s3,'-.','LineWidth',2,'Color',[0 0.7 0])
h7 = plot(s3,'^','MarkerSize',8,'LineWidth',2,'Color',[0 0.7 0]);

% for i = 1:length(e3)
%     me = 1.645*e3_me(i);
%     plot(i*[1 1],[e3(i)-me e3(i)+me],'-','LineWidth',1,'Color',[0 0.5 0])
%     plot(i*[1 1]+0.05*[-1 1],e3(i)+me*[1 1],'-','LineWidth',1,'Color',[0 0.5 0])
%     plot(i*[1 1]+0.05*[-1 1],e3(i)-me*[1 1],'-','LineWidth',1,'Color',[0 0.5 0])
% end

plot(s4,'k-.','LineWidth',2)
h8 = plot(s4,'ks','MarkerSize',8,'LineWidth',2);

% for i = 1:length(e4)
%     me = 1.645*e4_me(i);
%     plot(i*[1 1],[e4(i)-me e4(i)+me],'k-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e4(i)+me*[1 1],'k-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e4(i)-me*[1 1],'k-','LineWidth',1)
% end

% axis([0.5 4.5 0 0.15])
% axis([0.5 4.5 0 25])

% for i = 1:length(e1)
%     names{i} = [''];
% end

names{1} = ['C1, C5, C8, C10'];
names{2} = ['C2, C6, C9'];
names{3} = ['C3, C7'];
names{4} = ['C4'];

xlabel('Categories')
ylabel('Average SNR (dB)')

set(gca,'xtick',1:length(e1),'xticklabel',names)

% legend([h1 h2 h3 h4 h5 h6 h7 h8],{'Gap 1 (R)','Gap 2 (R)','Gap 3 (R)','Gap 4 (R)','Gap 1 (S)','Gap 2 (S)','Gap 3 (S)','Gap 4 (S)'})