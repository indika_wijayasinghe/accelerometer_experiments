close all
clear all
clc

N = 1000;

e1 = [0.09694850 0.10265411 0.11112533 0.11968232];
e1_me = [0.01281936 0.01532373 0.01753374 0.01681718]/sqrt(N);
s1 = [6.08418555 5.59775366 4.98788282 4.39157436];

e2 = [0.03550348 0.03696014 0.03929313];
e2_me = [0.00460329 0.00546938 0.00531563]/sqrt(N);
s2 = [13.64203669 13.32893851 12.93209541];

e3 = [0.02246016 0.02332781];
e3_me = [0.00294191 0.00316033]/sqrt(N);
s3 = [17.31498623 17.02849300];

e4 = [0.01679707];
e4_me = [0.00209523]/sqrt(N);
s4 = [19.72989611];

figure
hold on
plot(s1,'k-.')
h1 = plot(s1,'bo','MarkerSize',6,'LineWidth',2);

plot(s2,'k-.')
h2 = plot(s2,'rv','MarkerSize',6,'LineWidth',2);

plot(s3,'k-.')
h3 = plot(s3,'g^','MarkerSize',6,'LineWidth',2);

plot(s4,'k-.')
h4 = plot(s4,'cs','MarkerSize',6,'LineWidth',2);

axis([0.5 4.5 0 25])

% for i = 1:length(s1)
%     names{i} = [''];
% end

names{1} = ['C1, C5, C8, C10'];
names{2} = ['C2, C6, C9'];
names{3} = ['C3, C7'];
names{4} = ['C4'];

xlabel('Categories')
ylabel('Average SNR (dB)')

set(gca,'xtick',1:length(s1),'xticklabel',names)

legend([h1 h2 h3 h4],{'Gap 1','Gap 2','Gap 3','Gap 4'})