close all
clear all
clc

load('analyze_c.mat')

err_ex = mean(rerr_ex_2);
err_sim = mean(rerr_sim_2);
figure
yyaxis left
plot(err_ex,'Marker','o')
ylim([0.0188 0.0255])
yyaxis right
plot(err_sim,'Marker','o')
ylim([0.00495 0.0059])

snr_ex = mean(rsnr_ex_2);
snr_sim = mean(rsnr_sim_2);
figure
yyaxis left
plot(snr_ex,'Marker','o')
ylim([29.2 30.6])
yyaxis right
plot(snr_sim,'Marker','o')
ylim([29.0 30.9])

figure
plot(snr_ex,'Marker','o')
hold on
plot(snr_sim,'Marker','o')