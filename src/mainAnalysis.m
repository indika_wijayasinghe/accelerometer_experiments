close all
clear all
clc

f = 100;    % freq
t = 1/f;
enc_res = 2*pi/2^12*[1 1];
min_pks_dist = 3;

l{1} = 0.289;
l{2} = 0.289;
l_acc_num{1} = 2;
l_acc_num{2} = 2;

i = 1;
load(['../data/DATA_' num2str(i) '.mat'])
eval(['data = double(DATA_' num2str(i) ');']);

t_vec = 0:t:t*(size(data,1)-1);

acc_data = data(:,1:12);
enc_data = data(:,13:14).*enc_res;

% Start: Data preprocessing (could change in future versions)
for k = 1:2
    for j = 2:size(data,1)-1
        if abs(enc_data(j,k)-enc_data(j-1,k)) > 0.1 && abs(enc_data(j,k)-enc_data(j+1,k)) > 0.1
            enc_data(j,k) = (enc_data(j-1,k)+enc_data(j+1,k))/2;      
        end
    end
end

acc_placement = [1 2 4 3];                                                 % accelerometer permutations
R{1} = [1 0 0; 0 0 1; 0 -1 0];
R{2} = R{1};
R{3} = R{1}*[-1 0 0; 0 -1 0; 0 0 1];
R{4} = R{3};

for j = 1:4
    A{j} = acc_data(:,3*(acc_placement(j)-1)+1:3*acc_placement(j))*R{j}';  % coordinate transformation
end

[pks1,locs1] = findpeaks(enc_data(:,1),f,'MinPeakDistance',min_pks_dist);
[pks2,locs2] = findpeaks(enc_data(:,2),f,'MinPeakDistance',min_pks_dist);

if length(locs1) == length(locs2) + 1
    if abs(locs1(end) - locs2(end)) < min_pks_dist/2
        locs2 = [locs1(1); locs2];
        pks2 = [enc_data(locs1(1)*f,2); pks2];
    elseif abs(locs1(1) - locs2(1)) < min_pks_dist/2
        locs2 = [locs2; locs1(end)];
        pks2 = [pks2; enc_data(locs1(end)*f,2)];
    end
elseif length(locs2) == length(locs1) + 1
    if abs(locs1(end) - locs2(end)) < min_pks_dist/2
        locs1 = [locs2(1); locs1];
        pks1 = [enc_data(locs2(1)*f,2); pks1];
    elseif abs(locs1(1) - locs2(1)) < min_pks_dist/2
        locs1 = [locs1; locs2(end)];
        pks1 = [pks1; enc_data(locs2(end)*f,2)];
    end
end

% locs = (locs1 + locs2)/2;
locs = locs2;
pks1 = enc_data(int16(locs*f),1);
pks2 = enc_data(int16(locs*f),2);

offset = 12;

for j = 2:2
    for k = 1:4
        Trial(j-1).A{k} = A{k}(int16(locs(j)*f):int16(locs(j+offset)*f),:);
    end
    
    for k = 1:2
        Trial(j-1).E{k} = enc_data(int16(locs(j)*f):int16(locs(j+offset)*f),k);
    end
    
    Trial(j-1).T = double((int16(locs(j)*f):int16(locs(j+offset)*f)))/f;
end

% for j = 2:length(locs)-2
%     for k = 1:4
%         Trial(j-1).A{k} = A{k}(int16(locs(j)*f):int16(locs(j+1)*f),:);
%     end
%     
%     for k = 1:2
%          Trial(j-1).E{k} = enc_data(int16(locs(j)*f):int16(locs(j+1)*f),k);
%     end
%     
%     Trial(j-1).T = double((int16(locs(j)*f):int16(locs(j+1)*f)))/f;
% end
% End: Data preprocessing ends

trial_no = 1;

figure
subplot(2,1,1); plot(Trial(trial_no).T,Trial(trial_no).E{1},'b');
axis([Trial(trial_no).T(1) Trial(trial_no).T(end) 2.2 4])
title('Encoder 1')
xlabel('Time /(s)')
ylabel('Angle / (rad)')
subplot(2,1,2); plot(Trial(trial_no).T,Trial(trial_no).E{2},'b');
axis([Trial(trial_no).T(1) Trial(trial_no).T(end) 2.2 4])
title('Encoder 2')
xlabel('Time /(s)')
ylabel('Angle / (rad)')

% Estimation

T = Trial(trial_no).T';

l_acc_vec{1} = [0.087 0.188; 0.044 0.044; 0 0];
l_acc_vec{2} = [0.079 0.216; 0.047 0.047; 0 0];
% l{1}_acc_vec = [0.087 0.188; 0.2 0.2; 0 0];
% l{2}_acc_vec = [0.079 0.216; 0.2 0.2; 0 0];

accConfig.l{1} = l{1};
accConfig.l{2} = l{2};
accConfig.l_acc{1} = l_acc_vec{1};
accConfig.l_acc{2} = l_acc_vec{2};
accData = [Trial(trial_no).A{1}(:,1:2) Trial(trial_no).A{2}(:,1:2) Trial(trial_no).A{3}(:,1:2) Trial(trial_no).A{4}(:,1:2)];

Yr = obtainTrajFromAccelData(accData, accConfig);

% Yr = Yr + ones([size(Yr,1) 1])*([Trial(trial_no).E{1}(1) Trial(trial_no).E{2}(1)] + mean(Yr(1:1,:),1)+[1.4 -1.35]);
Yr = Yr + ones([size(Yr,1) 1])*([4.65 3.15]);

lowpassFilt = dsp.LowpassFilter('DesignForMinimumOrder',false, ...
    'FilterOrder',100,'PassbandFrequency',10,'SampleRate',100,...
    'PassbandRipple',0.01, 'StopbandAttenuation',80);

D = 0;

% D = mean(grpdelay(lowpassFilt));
% Yr(:,1) = lowpassFilt(Yr(:,1));
% Yr(:,2) = lowpassFilt(Yr(:,2));

Yr = Yr(D+1:end,:);

figure
subplot(2,1,1); plot(Trial(trial_no).T,Trial(trial_no).E{1},'b');
hold on; plot(Trial(trial_no).T(1:end-D),Yr(:,1),'r.');
% axis([Trial(trial_no).T(1) Trial(trial_no).T(end) 2.2 4])
title('Encoder 1')
xlabel('Time /(s)')
ylabel('Angle / (rad)')
subplot(2,1,2); plot(Trial(trial_no).T,Trial(trial_no).E{2},'b');
hold on; plot(Trial(trial_no).T(1:end-D),Yr(:,2),'r.');
% axis([Trial(trial_no).T(1) Trial(trial_no).T(end) 2.2 4])
title('Encoder 2')
xlabel('Time /(s)')
ylabel('Angle / (rad)')