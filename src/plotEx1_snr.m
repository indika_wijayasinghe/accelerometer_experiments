close all
clear all
clc

N = 1000;

e1 = [0.12876404 0.09387615 0.14141578 0.21682506];
e1_me = [0.01530273 0.01387199 0.00850469 0.00672695]/sqrt(N);
s1 = [6.81492744 9.89239405 10.07077459 12.91031587];

e2 = [0.06593684 0.07702664 0.08824530];
e2_me = [0.00849151 0.00660519 0.00500630]/sqrt(N);
s2 = [13.78763278 15.92117306 16.36715347];

e3 = [0.08102920 0.07711014];
e3_me = [0.00381431 0.00335703]/sqrt(N);
s3 = [17.39552754 19.15704871];

e4 = [0.06363787];
e4_me = [0.00296830]/sqrt(N);
s4 = [19.79991809];

figure
hold on
plot(s1,'k-.')
h1 = plot(s1,'bo','MarkerSize',6,'LineWidth',2);

plot(s2,'k-.')
h2 = plot(s2,'rv','MarkerSize',6,'LineWidth',2);

plot(s3,'k-.')
h3 = plot(s3,'g^','MarkerSize',6,'LineWidth',2);

plot(s4,'k-.')
h4 = plot(s4,'cs','MarkerSize',6,'LineWidth',2);

axis([0.5 4.5 0 25])

% for i = 1:length(s1)
%     names{i} = [''];
% end

names{1} = ['C1, C5, C8, C10'];
names{2} = ['C2, C6, C9'];
names{3} = ['C3, C7'];
names{4} = ['C4'];

xlabel('Categories')
ylabel('Average SNR (dB)')

set(gca,'xtick',1:length(s1),'xticklabel',names)

legend([h1 h2 h3 h4],{'Gap 1','Gap 2','Gap 3','Gap 4'})