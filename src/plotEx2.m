% close all
% clear all
% clc

er = mean(rerr_ex_2(:,1:4));
es = mean(rerr_sim_2(:,1:4));

% er = [0.0636 0.0634 0.0609 0.0606];
% es = [0.0167 0.0166 0.0147 0.0146];

names = {'C1', 'C2', 'C3', 'C4'};

subplot(1,2,1); plot(er,'ro','LineWidth',2); hold on; plot(er,'b--'); set(gca,'xtick',1:length(er),'xticklabel',names); set(gca,'ytick',0.055:0.005:0.065,'xticklabel',names); %axis([0.5 4.5 0.0599 0.0651]); 
xlabel('Categories'); ylabel('RMS Error (rad)'); title('Experiment')
subplot(1,2,2); plot(es,'ro','LineWidth',2); hold on; plot(es,'b--'); set(gca,'xtick',1:length(er),'xticklabel',names); set(gca,'ytick',0.010:0.004:0.018,'xticklabel',names); %axis([0.5 4.5 0.0139 0.0181]); 
xlabel('Categories'); ylabel('RMS Error (rad)'); title('Simulation')

figure 

er = mean(rsnr_ex_2(:,1:4));
es = mean(rsnr_sim_2(:,1:4));

% er = [19.79 19.75 20.60 20.58];
% es = [19.72 19.76 20.77 20.79];

names = {'C1', 'C2', 'C3', 'C4'};

subplot(1,2,1); plot(er,'ro','LineWidth',2); hold on; plot(er,'b--'); set(gca,'xtick',1:length(er),'xticklabel',names); set(gca,'ytick',18:0.5:21,'xticklabel',names); %axis([0.5 4.5 19.5 21]); 
xlabel('Categories'); ylabel('Average SNR (dB)'); title('Experiment')
subplot(1,2,2); plot(es,'ro','LineWidth',2); hold on; plot(es,'b--'); set(gca,'xtick',1:length(er),'xticklabel',names); set(gca,'ytick',18:0.5:21,'xticklabel',names); %axis([0.5 4.5 19.5 21]); 
xlabel('Categories'); ylabel('Average SNR (dB)'); title('Simulation')