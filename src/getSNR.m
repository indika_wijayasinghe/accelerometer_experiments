function snr = getSNR(signal, noise)
    snr = 1;
    
    signal_sum = sum(signal(:).^2);
    noise_sum = sum(noise(:).^2);
    
    snr = 10*log10(signal_sum/noise_sum);
end