% close all
% clear all
% clc

N = 1000;

% e1 = [0.12876404 0.09387615 0.14141578 0.21682506];
% e1_me = [0.01530273 0.01387199 0.00850469 0.00672695]/sqrt(N);
% s1 = [6.81492744 9.89239405 10.07077459 12.91031587];
% 
% e2 = [0.06593684 0.07702664 0.08824530];
% e2_me = [0.00849151 0.00660519 0.00500630]/sqrt(N);
% s2 = [13.78763278 15.92117306 16.36715347];
% 
% e3 = [0.08102920 0.07711014];
% e3_me = [0.00381431 0.00335703]/sqrt(N);
% s3 = [17.39552754 19.15704871];
% 
% e4 = [0.06363787];
% e4_me = [0.00296830]/sqrt(N);
% s4 = [19.79991809];

e1 = mean(rerr_ex_1(:,1:4));
e2 = mean(rerr_ex_1(:,5:7));
e3 = mean(rerr_ex_1(:,8:9));
e4 = mean(rerr_ex_1(:,10));

figure
hold on
plot(e1,'b-','LineWidth',2);
h1 = plot(e1,'bo','MarkerSize',8,'LineWidth',2);

% for i = 1:length(e1)
%     me = 1.645*e1_me(i);
%     plot(i*[1 1],[e1(i)-me e1(i)+me],'b-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e1(i)+me*[1 1],'b-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e1(i)-me*[1 1],'b-','LineWidth',1)
% end

plot(e2,'r-','LineWidth',2)
h2 = plot(e2,'rv','MarkerSize',8,'LineWidth',2);

% for i = 1:length(e2)
%     me = 1.645*e2_me(i);
%     plot(i*[1 1],[e2(i)-me e2(i)+me],'r-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e2(i)+me*[1 1],'r-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e2(i)-me*[1 1],'r-','LineWidth',1)
% end

plot(e3,'-','LineWidth',2,'Color',[0 0.7 0])
h3 = plot(e3,'^','MarkerSize',8,'LineWidth',2,'Color',[0 0.7 0]);

% for i = 1:length(e3)
%     me = 1.645*e3_me(i);
%     plot(i*[1 1],[e3(i)-me e3(i)+me],'-','LineWidth',1,'Color',[0 0.5 0])
%     plot(i*[1 1]+0.05*[-1 1],e3(i)+me*[1 1],'-','LineWidth',1,'Color',[0 0.5 0])
%     plot(i*[1 1]+0.05*[-1 1],e3(i)-me*[1 1],'-','LineWidth',1,'Color',[0 0.5 0])
% end

h11 = plot(e4,'k-','LineWidth',2);
h4 = plot(e4,'ks','MarkerSize',8,'LineWidth',2);

% for i = 1:length(e4)
%     me = 1.645*e4_me(i);
%     plot(i*[1 1],[e4(i)-me e4(i)+me],'k-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e4(i)+me*[1 1],'k-','LineWidth',1)
%     plot(i*[1 1]+0.05*[-1 1],e4(i)-me*[1 1],'k-','LineWidth',1)
% end

% axis([0.5 4.5 0 0.25])

% for i = 1:length(e1)
%     names{i} = [''];
% end

names{1} = ['C1, C5, C8, C10'];
names{2} = ['C2, C6, C9'];
names{3} = ['C3, C7'];
names{4} = ['C4'];

xlabel('Categories')
ylabel('RMS Error (rad)')

set(gca,'xtick',1:length(e1),'xticklabel',names)

% legend([h1 h2 h3 h4],{'Gap 1 (R)','Gap 2 (R)','Gap 3 (R)','Gap 4 (R)'})