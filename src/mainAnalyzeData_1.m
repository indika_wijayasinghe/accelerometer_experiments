function mainAnalyzeData
    close all
    clear all
    clc
    
    global freq min_pks_dist g enc_res acc_res accConfigEx sfreq sample_size accConfig accSimConfig
    
    % General parameters
    freq = 100; 
    sample_size = 1000;
    sfreq = 10000;
    g = 9.81;
    min_pks_dist = 6;
    enc_res = 2*pi/2^12*[1 1];
    acc_res = 2*9.81/2^15;
    acc_snr = 31;
    
    % Simulation specific parameters
    data_folder_str = 'dataset_1/experiment';
    trial_nos = 1; %4182; %5338;
    fig_orientation = 1;
    filter_acc_data = 0;
    filter_pos_data = 1;

    % Accelerometer config
    accConfig.l{1} = 0.2454;
    accConfig.l{2} = 0.2454;
    accConfig.l_num{1} = 5;
    accConfig.l_num{2} = 5;
%     accConfig.l_acc{1} = [0.04 0.08 0.12 0.16 0.20; 0.0215 0.0215 0.0215 0.0215 0.0215; 0 0 0 0 0];
%     accConfig.l_acc{2} = [0.04 0.08 0.12 0.16 0.20; 0.0215 0.0215 0.0215 0.0215 0.0215; 0 0 0 0 0];
    accConfig.l_acc{1} = [0.03998 0.05268 0.06538 0.07808 0.09078; 0.0215 0.0215 0.0215 0.0215 0.0215; 0 0 0 0 0];
    accConfig.l_acc{2} = [0.03998 0.05268 0.06538 0.07808 0.09078; 0.0215 0.0215 0.0215 0.0215 0.0215; 0 0 0 0 0];
    accConfig.l_num{1} = size(accConfig.l_acc{1},2);
    accConfig.l_num{2} = size(accConfig.l_acc{2},2);
    accConfig.l_ind{1} = [5 4 3 2 1];
    accConfig.l_ind{2} = [13 12 11 10 9];
    
    accSimConfig = accConfig;
    n_sensors = 5;
    accSimConfig.l_num{1} = n_sensors;
    accSimConfig.l_num{2} = n_sensors;
%     accSimConfig.l_acc{1} = [linspace(0.04, 0.20, n_sensors); 0.0215*ones(1,n_sensors); zeros(1,n_sensors)];
%     accSimConfig.l_acc{2} = [linspace(0.04, 0.20, n_sensors); 0.0215*ones(1,n_sensors); zeros(1,n_sensors)];
    accSimConfig.l_acc{1} = [linspace(0.03998, 0.09078, n_sensors); 0.0215*ones(1,n_sensors); zeros(1,n_sensors)];
    accSimConfig.l_acc{2} = [linspace(0.03998, 0.09078, n_sensors); 0.0215*ones(1,n_sensors); zeros(1,n_sensors)];
    
    % Acceleration config for experiment 1
    
    % Encoder config
    encConfig.zero_pos{1} = 1024+512;
    encConfig.zero_pos{2} = 1024;
    
    % Get accelerometer and encoder data
    senData = getSenData(accConfig, encConfig, data_folder_str, filter_acc_data);
    
% % %     % Plot encorder data
% % %     plotEncData(accConfig, senData, trial_nos, fig_orientation);
% % %     
% % %     % Plot accelerometer data
% % %     plotAccData(senData, accConfig, trial_nos);
    
    % Obtain trajectory values from accelerometer data    
    [estValues, noise] = obtainTrajFromAccelDataM1(senData, accConfig, trial_nos, filter_pos_data);
    estValues_1 = estValues;
    
    % Plot estimated values with encoder data
    plotEstValues(accConfig, senData, estValues, trial_nos, fig_orientation);
    
    % Obtain simulation data
    senDataS = getSimData(accSimConfig, senData, trial_nos, filter_acc_data, acc_snr);
    
    % Obtain trajectory values from simulated data
    [estSimValues,noiseSim] = obtainTrajFromAccelDataM1(senDataS, accSimConfig, trial_nos, filter_pos_data);
    
    % Plot simulation estimated values with encoder data
    plotEstValues(accSimConfig, senData, estSimValues, trial_nos, fig_orientation);
    
    close all
    testSNR
    
% % %     if length(trial_nos) == 1
% % %         [T,Yr] = obtainTrajFromAccelDataM2(senData, senData, accConfig, trial_nos);        
% % %         plotEstValues(accConfig, senData, Yr(:,1:2), trial_nos, fig_orientation);
% % %         
% % %         [T,YrSim] = obtainTrajFromAccelDataM2(senDataS, senData, accSimConfig, trial_nos);  
% % %         plotEstValues(accConfig, senData, YrSim(:,1:2), trial_nos, fig_orientation);
% % %     end

% %     %% Ex 0: Finding valid trials    
    accLoc{1} = ones(2,n_sensors);
    
%     [accConfigEx, senDataEx] = getAccConfigEx(accConfig, senData, accLoc{1});
    valid = ones([1 sample_size]);
    
% %     err = zeros([1 sample_size]);
% %     errI = zeros([1 sample_size]);
% %     
% %     for i = 1:sample_size
% %         clc
% %         fprintf('I: %d\n', i)
% %         estValues = obtainTrajFromAccelDataM1(senDataEx, accConfigEx, i, filter_pos_data);
% %         err(i) = getLeastSquareError(accConfigEx, senDataEx, estValues, i);
% %         errI(i) = getIntegralError(accConfigEx, senDataEx, estValues, i);
% %     end
% %     
% %     merr = mean(err);
% %     serr = std(err);
    
% %     for i = 1:length(err)
% %         if abs(err(i)-merr) < 3*serr
% %             valid(i) = 1;
% %         else
% %             valid(i) = 0;
% %         end
% %     end
% %     
% %     figure
% %     plot(err.*valid,'b-')
    
% %     %% Ex 1
% %     clear accLoc
% %     
% %     cnt = 1;
% %     
% %     v = 1:n_sensors; 
% %     
% %     for i = length(v):-1:1
% %         u = ~mod(v-1,i);
% %         
% %         if u(end)
% %             fprintf('I: %d\nU: [', i)
% %             
% %             for j = 1:length(u)
% %                 fprintf('%d ', u(j));
% %             end
% %             
% %             fprintf(']\n');
% %             
% %             accLoc{cnt} = ones([2 1])*u;
% %             cnt = cnt + 1;
% %         end
% %     end
% %     
% %     plotMarginOfError(accLoc, accSimConfig, senDataS, valid, 1, [0.5 3.5 0.000 0.025], filter_pos_data)

% % %     % Ex 1
% % %     clear accLoc
% % % 
% % %     accLoc{1} = ones([2 1])*[0 0 0 1 1];
% % %     accLoc{2} = ones([2 1])*[0 0 1 1 0];
% % %     accLoc{3} = ones([2 1])*[0 1 1 0 0];
% % %     accLoc{4} = ones([2 1])*[1 1 0 0 0];
% % %     accLoc{5} = ones([2 1])*[0 0 1 0 1];
% % %     accLoc{6} = ones([2 1])*[0 1 0 1 0];
% % %     accLoc{7} = ones([2 1])*[1 0 1 0 0];
% % %     accLoc{8} = ones([2 1])*[0 1 0 0 1];
% % %     accLoc{9} = ones([2 1])*[1 0 0 1 0];
% % %     accLoc{10} = ones([2 1])*[1 0 0 0 1];
% % %     
% % %     plotMarginOfError(accLoc, accSimConfig, senDataS, valid, 1, [0.5 3.5 0.000 0.025], filter_pos_data)
    
    % Ex 2
    clear accLoc

    accLoc{1} = ones([2 1])*[1 0 0 0 1];
    accLoc{2} = ones([2 1])*[1 0 1 0 1];
    accLoc{3} = ones([2 1])*[1 1 0 1 1];
    accLoc{4} = ones([2 1])*[1 1 1 1 1];
    
    plotMarginOfError(accLoc, accSimConfig, senDataS, valid, 2, [0.5 3.5 0.000 0.025], filter_pos_data)

% %     %% Ex 1
% %     clear accLoc
% % 
% %     accLoc{1} = ones([2 1])*[0 0 0 1 1];
% %     accLoc{2} = ones([2 1])*[0 0 1 1 0];
% %     accLoc{3} = ones([2 1])*[0 1 1 0 0];
% %     accLoc{4} = ones([2 1])*[1 1 0 0 0];
% %     accLoc{5} = ones([2 1])*[0 0 1 0 1];
% %     accLoc{6} = ones([2 1])*[0 1 0 1 0];
% %     accLoc{7} = ones([2 1])*[1 0 1 0 0];
% %     accLoc{8} = ones([2 1])*[0 1 0 0 1];
% %     accLoc{9} = ones([2 1])*[1 0 0 1 0];
% %     accLoc{10} = ones([2 1])*[1 0 0 0 1];
% %     
% %     plotMarginOfError(accLoc, accConfig, senData, valid, 1, [0.5 3.5 0.000 0.025], filter_pos_data)
% %     
% %     %% Ex 2
% %     clear accLoc
% %     
% %     accLoc{1} = ones([2 1])*[1 0 0 0 1];
% %     accLoc{2} = ones([2 1])*[1 0 1 0 1];
% %     accLoc{3} = ones([2 1])*[1 1 0 1 1];
% %     accLoc{4} = ones([2 1])*[1 1 1 1 1];
% %     
% %     plotMarginOfError(accLoc, accConfig, senData, valid, 2, [0.5 4.5 0.000 0.0056], filter_pos_data)
end

function plotMarginOfError(accLoc, accConfig, senData, valid, ExNo, axvec, filter_pos_data)
    global sample_size

    figure
    hold on
    [aerr_1, aerrI_1, rerr, rerrI, asnr, rsnr] = getErrorProfile(accLoc, accConfig, senData, valid, ExNo, filter_pos_data);
    plot(aerr_1(:,1),'b-.')
    plot(aerr_1(:,1),'ro','MarkerSize',3,'LineWidth',3)
    
    for i = 1:size(aerr_1,1)
        names{i} = ['C' num2str(i)];
    end
    
    set(gca,'xtick',1:size(aerr_1,1),'xticklabel',names)
%     axis(axvec)

     fprintf('ME: ');
    
    for i = 1:size(aerr_1,1)
%         me = 1.645*aerr_1(v(i),2);
        me = 1.96*aerr_1(i,2)/sqrt(sample_size);
        plot(i*[1 1],[aerr_1(i,1)-me aerr_1(i,1)+me],'r-','LineWidth',2)
        plot(i*[1 1]+0.05*[-1 1],aerr_1(i,1)+me*[1 1],'r-','LineWidth',2)
        plot(i*[1 1]+0.05*[-1 1],aerr_1(i,1)-me*[1 1],'r-','LineWidth',2)
        fprintf('%1.4f ', me);
    end
    
     fprintf('\n');
    
    saveas(gcf, ['Ex_' num2str(ExNo) '_err'], 'fig')
    
    figure
    hold on
    plot(asnr(:,1),'b-.')
    plot(asnr(:,1),'ro','MarkerSize',3,'LineWidth',3)
    
    set(gca,'xtick',1:size(aerr_1,1),'xticklabel',names)
    
    saveas(gcf, ['Ex_' num2str(ExNo) '_snr'], 'fig')
end

function [aerr, aerrI, rerr, rerrI, asnr, rsnr] = getErrorProfile(accLoc, accConfig, senData, valid, exNo, filter_pos_data)
    global sample_size

    fprintf('Experiment %d:\n', exNo)

    aerr = zeros([size(accLoc,2) 3]);
    aerrI = zeros([size(accLoc,2) 3]);
    asnr = zeros([size(accLoc,2) 3]);
    rerr = zeros([sample_size size(accLoc,2)]);
    rerrI = zeros([sample_size size(accLoc,2)]);
    rsnr = zeros([sample_size size(accLoc,2)]);

    for k = 1:size(accLoc,2)
        fprintf('-------------------------------\n')
        fprintf('Configuration: [%d %d %d %d %d]\n', accLoc{k}(1,1), accLoc{k}(1,2), accLoc{k}(1,3), accLoc{k}(1,4), accLoc{k}(1,5))
        [accConfigEx, senDataEx] = getAccConfigEx(accConfig, senData, accLoc{k});

        err = zeros([1 sample_size]);
        errI = zeros([1 sample_size]);
        
        snr = zeros([1 sample_size]);

        for i = 1:sample_size
            [estValues, noise] = obtainTrajFromAccelDataM1(senDataEx, accConfigEx, i, filter_pos_data);
            
            snr(i) = getSNR(estValues,noise);            
            err(i) = getLeastSquareError(accConfigEx, senDataEx, estValues, i);
            errI(i) = getIntegralError(accConfigEx, senDataEx, estValues, i);
        end
        
        rerr(:,k) = err(valid == 1)';
        rerrI(:,k) = errI(valid == 1)';
        rsnr(:,k) = snr(valid == 1)';

        aerr(k,:) = [mean(err(valid == 1)) std(err(valid == 1)) sum(valid)];
        aerrI(k,:) = [mean(errI(valid == 1)) std(errI(valid == 1)) sum(valid)];
        asnr(k,:) = [mean(snr(valid == 1)) std(snr(valid == 1)) sum(valid)];

        fprintf('LS Err: [%1.8f %1.8f]\n', mean(err(valid == 1)), std(err(valid == 1)));
        fprintf('SNR: [%1.8f %1.8f]\n', mean(snr(valid == 1)), std(snr(valid == 1)));
%         fprintf('In Err: [%1.8f %1.8f]\n', mean(errI(valid == 1)), std(errI(valid == 1)));
        fprintf('Validity: %d / %d\n', sum(valid), length(err));
    end

    fprintf('-------------------------------\n')
end

function snr = getSNR(signal, noise)
    snr = 1;
    
    signal_sum = sum(signal(:).^2);
    noise_sum = sum(noise(:).^2);
    
    snr = 10*log10(signal_sum/noise_sum);
end

function [accCon, senD] = getAccConfigEx(accConfig, senData, accloc)
    accCon.l{1} = accConfig.l{1};
    accCon.l{2} = accConfig.l{2};
    accCon.l_num{1} = sum(accloc(1,:));
    accCon.l_num{2} = sum(accloc(2,:));
    accCon.l_acc{1} = accConfig.l_acc{1}(:,accloc(1,:) == 1);
    accCon.l_acc{2} = accConfig.l_acc{2}(:,accloc(2,:) == 1);
    accCon.l_num{1} = size(accCon.l_acc{1},2);
    accCon.l_num{2} = size(accCon.l_acc{2},2);
    accCon.l_ind{1} = accConfig.l_ind{1};
    accCon.l_ind{2} = accConfig.l_ind{2};
    
    if isfield(senData,'Trial')
        for i = 1:size(senData.Trial,2)
            senD.Trial(i).T = senData.Trial(i).T;
            senD.Trial(i).E = senData.Trial(i).E;

            cnt = 1;

            for j = 1:2*size(accloc,2)
                if accloc(ceil(j/size(accloc,2)),mod(j-1,size(accloc,2))+1) 
                    senD.Trial(i).A{cnt} = senData.Trial(i).A{j};
                    cnt = cnt + 1;
                end
            end
        end
    else
        senD = senData;
    end
end

function err = checkErr(senData, accConfigEx, trial_nos)
    [comData, ~, ~] = combineData(senData, accConfigEx, trial_nos);
    
    v = zeros([1 size(comData,2)]);
    
    for i = 1:size(comData,2)
        v(i) = max(diff(comData(:,i)));
    end
    
    err = max(v);
end

function err = getIntegralError(accConfigEx, senData, estValues, trial_nos)
    global freq

    [~, comEnc, ~] = combineData(senData, accConfigEx, trial_nos);
    
    errValues = abs(estValues - comEnc);
    
    err = 0;
    
    for i = 1:size(errValues,2)
        err = err + trapz(errValues(:,i))/freq;
    end
end

function err = getLeastSquareError(accConfigEx, senData, estValues, trial_nos)
    [~, comEnc, ~] = combineData(senData, accConfigEx, trial_nos);
    errEnc = (estValues - comEnc);
    err = sqrt((sum(errEnc(:,1).^2) + sum(errEnc(:,2).^2))/(2*size(errEnc,1)));
end

function [T,Y] = obtainTrajFromAccelDataM2(comDataA, senDataA, accConfigExA, trial_nos)
    global accConfigEx comData comT sfreq accConfig
    
    accConfigEx = accConfig;
    
    [comData, comEnc, comT] = combineData(senDataA, accConfigEx, trial_nos);
    
    if ~isfield(comDataA, 'Trial')        
        accConfigEx = accConfigExA;
        
        comData = [];
    
        for i = 1:length(comDataA.A)
            comData = [comData comDataA.A{i}];
        end
    end
    
    initx = [comEnc(1,1) comEnc(1,2) (comEnc(2,1)-comEnc(1,1))/0.01 (comEnc(2,2)-comEnc(1,2))/0.01];    
%     options = odeset('RelTol', 1e-6, 'AbsTol',[1e-9 1e-9 1e-9 1e-9]);
    [T,Y] = ode4(@inverseDynamicsL1L2, 0:1/sfreq:1, initx);
end

function d = load_data(str)
    listing = dir(['../data/' str]);
    d = [];
    
    for i = 1:50 %size(listing,1)-2
        str_name = ['DATA_' num2str(i) '.mat'];
        
        try
            load(['../data/' str '/' str_name]);
            eval(['d = [d; ' str_name(1:end-4) '];'])
        catch
            fprintf(['Error loading ' str_name '\n']);
        end
    end
end

function [filSig, noise] = filterSignal(cData)
    d = designfilt('lowpassfir', 'FilterOrder', 100, ...
                 'CutoffFrequency', 3, 'SampleRate', 100);
             
    filSig = zeros(size(cData));
    
    for i = 1:size(cData,2)        
        filSig(:,i) = filtfilt(d,cData(:,i));
    end
    
    noise = cData - filSig;
end

function plotAccData(senData, accConfigEx, trial_nos)
    [comData, ~, comT] = combineData(senData, accConfigEx, trial_nos);
    acc_no = size(comData,2)/2;
    
    figure

    for i = 1:acc_no        
        subplot(2,acc_no,i);
        plot(comT,comData(:,2*i-1));
        hold on
        subplot(2,acc_no,acc_no+i);
        plot(comT,comData(:,2*i));        
        hold on
    end
end

function accSimData = getSimData(accConfigEx, senData, trial_no, filter_acc_data, acc_snr)
    global qv qvd qvdd g ddxv_l1_n ddxv_l2_n sample_size
    
    l1 = accConfigEx.l{1};
    l2 = accConfigEx.l{2};
    l1_vec = [l1 0 0]';
    l2_vec = [l2 0 0]';
    l1_num = accConfigEx.l_num{1};
    l2_num = accConfigEx.l_num{2};
    l1_ap_vec = accConfigEx.l_acc{1};
    l2_ap_vec = accConfigEx.l_acc{2};
    inv_snr_l1 = 0;
    inv_snr_l2 = 0;
    
    odetype = 2;
    
    for k = 1:sample_size
        clc
        fprintf('Sim Trial: %d\n', k)
        
        T = [];
        enc_data = [];

        T = [T; senData.Trial(k).T];
        enc_data = [enc_data; senData.Trial(k).E{1} senData.Trial(k).E{2}];

        qv = [enc_data T];
        [qvd, qvdd] = getDerivatives(qv(:,1:2),T);

        Y = [qv qvd];

        l1_ap = zeros([1 l1_num]);
        l2_ap = zeros([1 l2_num]);

        al = zeros([1 l1_num+l2_num]);
        alg = zeros([1 l1_num]);

        for i = 1:l1_num
            l1_ap(i) = norm(l1_ap_vec(:,i),2);
            l2_ap(i) = norm(l2_ap_vec(:,i),2);

            il1_vec(:,i) = l1_ap_vec(:,i) - l1_vec;

            al(i) = atan2(il1_vec(2,i),abs(il1_vec(1,i)));    
            al(l1_num+i) = atan(l2_ap_vec(2,i)/l2_ap_vec(1,i));
            alg(i) = atan(l1_ap_vec(2,i)/l1_ap_vec(1,i));

            Ra{i} = [cos(al(i)) -sin(al(i)); sin(al(i)) cos(al(i))];
            Ra{i+l1_num} = [cos(al(i+l1_num)) -sin(al(i+l1_num)); sin(al(i+l1_num)) cos(al(i+l1_num))];
            Rag{i} = [cos(alg(i)) -sin(alg(i)); sin(alg(i)) cos(alg(i))];
        end

        accL1 = zeros(length(T),4);
        accL2 = zeros(length(T),4);

        for i = 1:length(T)
            accL1(i,:) = getAccelerationL1(i, accConfigEx)';
            accL2(i,:) = getAccelerationL2(i, accConfigEx)';
        end

        ddxv_l1 = zeros([length(T) 2*l1_num+1]);
        ddxv_l2 = zeros([length(T) 2*l2_num+1]);

        ddxv_l1_t = zeros([length(T) 2]);
        ddxv_l2_t = zeros([length(T) 2]);

        if odetype == 2
            for i = 1:l1_num
                ddxv_l1(:,2*i-1:2*i) = ((accL1(:,3:4)*norm(l1_ap_vec(1:2,i),2)/l1).*(1+2*(rand([length(T) 2])-0.5)*inv_snr_l1));
            end

            for i = 1:l2_num
                ddxv_l2(:,2*i-1:2*i) = (((accL2(:,3:4)-accL1(:,3:4))*norm(l2_ap_vec(1:2,i),2)/l2 + accL1(:,3:4)).*(1+2*(rand([length(T) 2])-0.5)*inv_snr_l2));
            end

            for i = 1:length(T)
                ang = -Y(i,1);
                R1 = [cos(ang) -sin(ang); sin(ang) cos(ang)];
                ang = -(Y(i,1) + Y(i,2));
                R2 = [cos(ang) -sin(ang); sin(ang) cos(ang)];

                ddxv_l1_t(i,1:2) = (accL1(i,3:4)+[0 -g])*R1';
                ddxv_l2_t(i,1:2) = (accL2(i,3:4)+[0 -g])*R2';

                for j = 1:l1_num
                    ddxv_l1(i,2*j-1:2*j) = (ddxv_l1(i,2*j-1:2*j)*Rag{j}'+[0 g])*R1';
    %                 ddxv_l1(i,2*j-1:2*j) = (ddxv_l1(i,2*j-1:2*j)*Rag{j}'+[0 -g])*R1';
                end

                for j = 1:l2_num
                    ddxv_l2(i,2*j-1:2*j) = ((ddxv_l2(i,2*j-1:2*j)-accL1(i,3:4))*Ra{j+l1_num}'+accL1(i,3:4)+[0 g])*R2';
    %                 ddxv_l2(i,2*j-1:2*j) = ((ddxv_l2(i,2*j-1:2*j)-accL1(i,3:4))*Ra{j+l1_num}'+accL1(i,3:4)+[0 -g])*R2';
                end
            end
        else        
            for i = 1:l1_num
                ddxv_l1(:,2*i-1:2*i) = accL1(:,3:4)*l1_ap(i)/l1;
            end

            for i = 1:l2_num
                ddxv_l2(:,2*i-1:2*i) = (accL2(:,3:4)-accL1(:,3:4))*l2_ap(i)/l2 + accL1(:,3:4);
            end
        end

        for i = 2:size(ddxv_l1,1)-1
            for j = 1:size(ddxv_l1,2)
                if isnan(ddxv_l1(i,j))
                    ddxv_l1(i,j) = (ddxv_l1(i-1,j)+ddxv_l1(i+1,j))/2;
                end
            end
        end

        for i = 2:size(ddxv_l2,1)-1
            for j = 1:size(ddxv_l2,2)
                if isnan(ddxv_l2(i,j))
                    ddxv_l2(i,j) = (ddxv_l2(i-1,j)+ddxv_l2(i+1,j))/2;
                end
            end
        end

    %     if filter_acc_data
            [ddxv_l1,~] = filterSignal(ddxv_l1);
            [ddxv_l2,~] = filterSignal(ddxv_l2);
    %     end

        ddxv_l1(:,end) = T;
        ddxv_l2(:,end) = T;

        for i = 1:l1_num
%             accSimData.Trial(k).A{i} = ddxv_l1(:,2*i-1:2*i)+12.5*sqrt(1/(10^(acc_snr/10)))*2*(randn(size(ddxv_l1,1),2)-0.5);
            accSimData.Trial(k).A{i} = ddxv_l1(:,2*i-1:2*i).*(1+sqrt(1/(10^(acc_snr/10)))*randn(size(ddxv_l1,1),2));
    %         [acc_sig,noise] = filterSignal(accSimData.A{i});
    %         snr = getSNR(acc_sig,noise);
        end

        for i = 1:l2_num
%             accSimData.Trial(k).A{l1_num+i} = ddxv_l2(:,2*i-1:2*i)+12.5*sqrt(1/(10^(acc_snr/10)))*2*(randn(size(ddxv_l1,1),2)-0.5);
            accSimData.Trial(k).A{l1_num+i} = ddxv_l2(:,2*i-1:2*i).*(1+sqrt(1/(10^(acc_snr/10)))*randn(size(ddxv_l2,1),2));
    %         [acc_sig,noise] = filterSignal(accSimData.A{i});
    %         snr = getSNR(acc_sig,noise);
        end

        accSimData.Trial(k).E{1} = senData.Trial(k).E{1};
        accSimData.Trial(k).E{2} = senData.Trial(k).E{2};

        accSimData.Trial(k).T = T;
    end

        ddxv_l1_n = ddxv_l1;
        ddxv_l2_n = ddxv_l2;
end

function [comData, comEnc, comT] = combineData(senData, accConfigEx, trial_no)
    l1_num = accConfigEx.l_num{1};
    l2_num = accConfigEx.l_num{2};

    comT = [];
    comData = [];
    comEnc = [];

    for j = 1:length(trial_no)
        acc_data = [];
        enc_data = [];
        
        for i = 1:(l1_num+l2_num)
            if isfield(senData, 'Trial')
                temp = senData.Trial(trial_no(j)).A{i};
            else
                temp = senData.A{i};
            end
%             acc_data = [acc_data sqrt(temp(:,1).^2+temp(:,3).^2)  temp(:,2)];
            acc_data = [acc_data temp(:,1) temp(:,2)];
        end
        
        for i = 1:length(accConfigEx.l)
            enc_data = [enc_data senData.Trial(trial_no(j)).E{i}];
        end
        
        comData = [comData; acc_data];
        comEnc = [comEnc; enc_data];
        comT = [comT; senData.Trial(trial_no(j)).T];
    end
end

function [qd, qdd] = getDerivatives(q,t)
    qph = circshift(q,[-1 0]);
    qmh = circshift(q,[1 0]);
    
    h = (t - circshift(t,[1 0]));
    
    qd = (qph-qmh)./(2*h*[1 1]);
    qdd = (qph-2*q+qmh)./(h.^2*[1 1]);
    
    qd(1,:) = qd(2,:);
    qdd(1,:) = qdd(2,:);
    qd(end,:) = qd(end-1,:);
    qdd(end,:) = qdd(end-1,:);
end

function dx = getAccelerationL1(i, accConfigEx)
    global l1 qv qvd qvdd
    
    l1 = accConfigEx.l{1};
    
    dx = zeros(4,1);
    
    q = qv(i,1:2)';
    q(3:4) = qvd(i,:)';
    t = qv(i,3);

    J1 = [-l1*sin(q(1)) 0; l1*cos(q(1)) 0];
    
    dJ1dq1 = [-l1*cos(q(1)) 0; -l1*sin(q(1)) 0];
    
    dJ1dt = dJ1dq1*q(3);
    
%     qdd = twolinkMass(t,q)\twolinkDynamics(t,q);
    qdd = [0 0 0 0]';

%     q(3:4) = qv(i,3:4)';
    qdd(3:4) = qvdd(i,:)';

    dx(1:2) = J1*q(3:4);
    dx(3:4) = dJ1dt*q(3:4) + J1*qdd(3:4);
end

function dx = getAccelerationL2(i, accConfigEx)
    global l1 l2 qv qvd qvdd
    
    l1 = accConfigEx.l{1};
    l2 = accConfigEx.l{2};
    
    dx = zeros(4,1);
    
    q = qv(i,1:2)';
    q(3:4) = qvd(i,:)';
    t = qv(i,3);

    J2 = [-l1*sin(q(1))-l2*sin(q(1)+q(2)) -l2*sin(q(1)+q(2)); l1*cos(q(1))+l2*cos(q(1)+q(2)) l2*cos(q(1)+q(2))];
    
    dJ2dq1 = [-l1*cos(q(1))-l2*cos(q(1)+q(2)) -l2*cos(q(1)+q(2)); -l1*sin(q(1))-l2*sin(q(1)+q(2)) -l2*sin(q(1)+q(2))];
    dJ2dq2 = [-l2*cos(q(1)+q(2)) -l2*cos(q(1)+q(2)); -l2*sin(q(1)+q(2)) -l2*sin(q(1)+q(2))];
    
    dJ2dt = dJ2dq1*q(3) + dJ2dq2*q(4);
    
%     qdd = twolinkMass(t,q)\twolinkDynamics(t,q);
    qdd = [0 0 0 0]';

%     q(3:4) = qv(i,3:4)';
    qdd(3:4) = qvdd(i,:)';
    
    dx(1:2) = J2*q(3:4);
    dx(3:4) = dJ2dt*q(3:4) + J2*qdd(3:4);
end

function plotEstValues(accConfigEx, senData, estValues, trial_no, fig_orientation)
    global sfreq

    figure

    for i = 1:length(accConfigEx.l)
        if fig_orientation == 1
            subplot(1,length(accConfigEx.l),i);
        elseif fig_orientation == 2
            subplot(length(accConfigEx.l),1,i);
        end
        
        T = [];
        enc_data = [];
        
        for j = 1:length(trial_no)
            T = [T; senData.Trial(trial_no(j)).T];
            enc_data = [enc_data; senData.Trial(trial_no(j)).E{i}];
        end
        
        if length(T) == size(estValues,1)
            Ts = T;
        else
            Ts = T(1):1/sfreq:((size(estValues,1)-1)/sfreq + T(1));
        end
        
        plot(Ts,estValues(:,i),'r-');
        hold on;
        plot(T,enc_data,'b');
        axis([T(1) T(end) floor(min(enc_data)) ceil(max(enc_data))])
        title(['Encoder ' num2str(i)])
        xlabel('Time /(s)')
        ylabel('Angle / (rad)')
    end
end

function plotEncData(accConfigEx, senData, trial_no, fig_orientation)
    figure

    for i = 1:length(accConfigEx.l)
        if fig_orientation == 1
            subplot(1,length(accConfigEx.l),i);
        elseif fig_orientation == 2
            subplot(length(accConfigEx.l),1,i);
        end
        
        T = [];
        enc_data = [];
        
        for j = 1:length(trial_no)
            T = [T; senData.Trial(trial_no(j)).T];
            enc_data = [enc_data; senData.Trial(trial_no(j)).E{i}];
        end
        
        plot(T,enc_data,'b');
        axis([T(1) T(end) floor(min(enc_data)) ceil(max(enc_data))])
        title(['Encoder ' num2str(i)])
        xlabel('Time /(s)')
        ylabel('Angle / (rad)')
    end
end

function senDataR = getSenData(accConfigEx, encConfig, data_folder_str, filter_acc_data)
    global freq min_pks_dist enc_res
    
    if exist(['../data/' data_folder_str '/' data_folder_str '.mat'])
        load(['../data/' data_folder_str '/' data_folder_str '.mat'])
    else
        data = double(load_data(data_folder_str));
%         find_calibrate_parameters(data(1:1300000,:));
        data = calibrate(data);
        acc_data = data(:,1:48);
        enc_data = -(double(data(:,49:50))-ones([size(data,1) 1])*[encConfig.zero_pos{1} encConfig.zero_pos{2}]).*(ones([size(data,1) 1])*enc_res);
%         save(['../data/' data_folder_str '/' data_folder_str '.mat'], 'acc_data', 'enc_data');
    end
    
    noise = zeros(size(acc_data));
    
    if filter_acc_data
        [acc_data, noise] = filterSignal(acc_data);
        snr = getSNR(acc_data(:,[1 3]),noise(:,[1 3]));
    end
    
    for k = 1:2
        for j = 2:size(acc_data,1)-1
            if abs(enc_data(j,k)-enc_data(j-1,k)) > 0.1 && abs(enc_data(j,k)-enc_data(j+1,k)) > 0.1
                enc_data(j,k) = (enc_data(j-1,k)+enc_data(j+1,k))/2;
            end
        end
    end
    
    acc_placement = [accConfigEx.l_ind{1} accConfigEx.l_ind{2}];                                                 % accelerometer permutations
    R{1} = [1 0 0; 0 0 1; 0 -1 0]*[-1 0 0; 0 -1 0; 0 0 1];
    R{2} = R{1};
    R{3} = R{1};
    R{4} = R{1};
    R{5} = R{1};
    R{6} = R{1};
    R{7} = R{1};
    R{8} = R{1};
    R{9} = R{1};
    R{10} = R{1};
    R{11} = R{1};
    R{12} = R{1};
    R{13} = R{1};
    R{14} = R{1};
    R{15} = R{1};
    R{16} = R{1};
    
    for j = 1:length(acc_placement)
        A{j} = acc_data(:,3*(acc_placement(j)-1)+1:3*acc_placement(j))*R{j}';  % coordinate transformation
        N{j} = noise(:,3*(acc_placement(j)-1)+1:3*acc_placement(j))*R{j}';  % coordinate transformation
    end
    
    [pks1,locs1] = findpeaks(enc_data(:,1),freq,'MinPeakDistance',min_pks_dist);
    [pks2,locs2] = findpeaks(enc_data(:,2),freq,'MinPeakDistance',min_pks_dist);
    
    if length(locs1) == length(locs2) + 1
        if abs(locs1(end) - locs2(end)) < min_pks_dist/2
            locs2 = [locs1(1); locs2];
            pks2 = [enc_data(locs1(1)*freq,2); pks2];
        elseif abs(locs1(1) - locs2(1)) < min_pks_dist/2
            locs2 = [locs2; locs1(end)];
            pks2 = [pks2; enc_data(locs1(end)*freq,2)];
        end
    elseif length(locs2) == length(locs1) + 1
        if abs(locs1(end) - locs2(end)) < min_pks_dist/2
            locs1 = [locs2(1); locs1];
            pks1 = [enc_data(locs2(1)*freq,2); pks1];
        elseif abs(locs1(1) - locs2(1)) < min_pks_dist/2
            locs1 = [locs1; locs2(end)];
            pks1 = [pks1; enc_data(locs2(end)*freq,2)];
        end
    end
    
    locs = (locs1 + locs2)/2;
    %     locs = locs1;
    pks1 = enc_data(int32(locs*freq),1);
    pks2 = enc_data(int32(locs*freq),2);
    
    for j = 2:length(locs)-2
        for k = 1:length(acc_placement)
            senDataR.Trial(j-1).A{k} = A{k}(int32(locs(j)*freq):int32(locs(j+1)*freq),:);
            senDataR.Trial(j-1).N{k} = N{k}(int32(locs(j)*freq):int32(locs(j+1)*freq),:);
        end
        
        for k = 1:2
            senDataR.Trial(j-1).E{k} = enc_data(int32(locs(j)*freq):int32(locs(j+1)*freq),k);
        end
        
        senDataR.Trial(j-1).T = double((int32(locs(j)*freq):int32(locs(j+1)*freq)))'/freq;
    end
end

function datac = calibrate(data)
    load('../data/dataset_1/pks_zrs.mat');
    
    R1 = [0 0 1; 0 1 0; -1 0 0];
    R2 = [1 0 0; 0 0 -1; 0 1 0];
    
    for i = 1:16
        data(:,3*(i-1)+1:3*(i-1)+3) = (R2*R1*data(:,3*(i-1)+1:3*(i-1)+3)')';
    end
    
    for i = 1:16
       R_m = [cos(th_m(i)) 0 sin(th_m(i)); 0 1 0; -sin(th_m(i)) 0 cos(th_m(i))];
       data(:,3*(i-1)+1:3*(i-1)+3) = (R_m*data(:,3*(i-1)+1:3*(i-1)+3)')';
    end
    
    for i = 1:3:48
        datac(:,i) = -9.81/(pks(i)-zrs(i))*(data(:,i)-zrs(i));
    end
    
    for i = 3:3:48
        datac(:,i) = 9.81/(pks(i)-zrs(i))*(data(:,i)-zrs(i));
    end
    
    datac = [datac data(:,49:50)];
end

function find_calibrate_parameters(data)
%     lim_e = 122000;
    lim_e = 0;
    lim_s = 200;
    
    T = 0:0.01:(size(data,1)-1)*0.01;
    
    d = designfilt('lowpassfir', 'FilterOrder', 1000, ...
                 'CutoffFrequency', 0.00001, 'SampleRate', 10);
             
    for i = 1:size(data,2)-2
        dataf(:,i) = filtfilt(d,data(:,i));
    end

    subplot(2,1,1);
    plot(T(1:end-lim_e),data(1:end-lim_e,49));
    subplot(2,1,2);
    plot(T(1:end-lim_e),data(1:end-lim_e,50));
    
%     a = 2;
%     l = 1;
    
    R1 = [0 0 1; 0 1 0; -1 0 0];
    R2 = [1 0 0; 0 0 -1; 0 1 0];
    
    for i = 1:16
        dataf(:,3*(i-1)+1:3*(i-1)+3) = (R2*R1*dataf(:,3*(i-1)+1:3*(i-1)+3)')';
        data(:,3*(i-1)+1:3*(i-1)+3) = (R2*R1*data(:,3*(i-1)+1:3*(i-1)+3)')';
    end
    
    for i = 1:16
        [pks(3*(i-1)+1), pks_t(3*(i-1)+1)] = min(dataf(1+lim_s:end-lim_e,3*(i-1)+1));
        zrs(3*(i-1)+3) = dataf(pks_t(3*(i-1)+1),3*(i-1)+3);
        zrs_t(3*(i-1)+3) = pks_t(3*(i-1)+1);
    end
    
    for i = 1:16
        [pks(3*(i-1)+3), pks_t(3*(i-1)+3)] = max(dataf(1+lim_s:end-lim_e,3*(i-1)+3));
        zrs(3*(i-1)+1) = dataf(pks_t(3*(i-1)+3),3*(i-1)+1);
        zrs_t(3*(i-1)+1) = pks_t(3*(i-1)+3);
    end
    
    for i = 1:3:48
        datac(:,i) = -9.81/(pks(i)-zrs(i))*(data(:,i)-zrs(i));
    end
    
    for i = 3:3:48
        datac(:,i) = 9.81/(pks(i)-zrs(i))*(data(:,i)-zrs(i));
    end
             
    for i = 1:size(data,2)-2
        datafc(:,i) = filtfilt(d,datac(:,i));
    end
    
    %%
    temp = 1:size(data,1);
    k = temp(data(:,49) == 1024+512);
    k = k(1);
    l = temp(data(:,50) == 1024);
    l = l(1);

    enc_m = zeros([1 16]);
    th_m = zeros([1 16]);

    for i = 1:16
       enc_m(i) = data(pks_t(3*(i-1)+3),49); 
       th_m(i) = (enc_m(i)-1536)*pi/2048;
       R_m = [cos(th_m(i)) 0 sin(th_m(i)); 0 1 0; -sin(th_m(i)) 0 cos(th_m(i))];
       datafc(:,3*(i-1)+1:3*(i-1)+3) = (R_m*datafc(:,3*(i-1)+1:3*(i-1)+3)')';
       datac(:,3*(i-1)+1:3*(i-1)+3) = (R_m*datac(:,3*(i-1)+1:3*(i-1)+3)')';
    end
    %%
    
    figure
    
    for i = 1:16
        subplot(4,4,i);
        hold on
        plot(T(1:end-lim_e),datac(1:end-lim_e,3*(i-1)+1),'b');
        plot(T(1:end-lim_e),datafc(1:end-lim_e,3*(i-1)+1),'r');
        plot(T(l),-9.81,'gd');
        plot(T(k),0,'go');
    end
    
    figure
    
    for i = 1:16
        subplot(4,4,i);
        hold on
        plot(T(1:end-lim_e),datac(1:end-lim_e,3*(i-1)+3),'b');
        plot(T(1:end-lim_e),datafc(1:end-lim_e,3*(i-1)+3),'r');
        plot(T(k),9.81,'gd');
        plot(T(l),0,'go');
    end
    
    save('../data/pks_zrs.mat','pks','zrs','th_m');
end

function dq = inverseDynamicsL1L2(t,q)
    global ddxv_l1 ddxv_l2 g accConfigEx comData comT
    
    l1 = accConfigEx.l{1};
    l2 = accConfigEx.l{2};
    l1_vec = [l1 0 0]';
    l2_vec = [l2 0 0]';
    l1_num = accConfigEx.l_num{1};
    l2_num = accConfigEx.l_num{2};
    l1_ap_vec = accConfigEx.l_acc{1};
    l2_ap_vec = accConfigEx.l_acc{2};
    inv_snr_l1 = 0;
    inv_snr_l2 = 0;
    
    l1_ap = zeros([1 l1_num]);
    l2_ap = zeros([1 l2_num]);
    
    al = zeros([1 l1_num+l2_num]);
    alg = zeros([1 l1_num]);
    
    ddxv_l1 = [comData(:,1:2*l1_num) comT-comT(1)];
    ddxv_l2 = [comData(:,2*l1_num+1:end) comT-comT(1)];
    
    for i = 1:l1_num
        l1_ap(i) = norm(l1_ap_vec(:,i),2);
        l2_ap(i) = norm(l2_ap_vec(:,i),2);
        
        il1_vec(:,i) = l1_ap_vec(:,i) - l1_vec;
        
        al(i) = atan2(il1_vec(2,i),abs(il1_vec(1,i)));    
        al(l1_num+i) = atan(l2_ap_vec(2,i)/l2_ap_vec(1,i));
        alg(i) = atan(l1_ap_vec(2,i)/l1_ap_vec(1,i));
        
        Ra{i} = [cos(al(i)) -sin(al(i)); sin(al(i)) cos(al(i))];
        Ra{i+l1_num} = [cos(al(i+l1_num)) -sin(al(i+l1_num)); sin(al(i+l1_num)) cos(al(i+l1_num))];
        Rag{i} = [cos(alg(i)) -sin(alg(i)); sin(alg(i)) cos(alg(i))];
    end
    
    dq = zeros(4,1);
    ddx = zeros(size(ddxv_l1,2)+size(ddxv_l2,2)-2,1);
    J = zeros(size(ddxv_l1,2)+size(ddxv_l2,2)-2,2);
    dJdt = zeros(size(ddxv_l1,2)+size(ddxv_l2,2)-2,2);

    J1 = [-l1*sin(q(1)) 0; l1*cos(q(1)) 0];
    J2 = [-l1*sin(q(1))-l2*sin(q(1)+q(2)) -l2*sin(q(1)+q(2)); l1*cos(q(1))+l2*cos(q(1)+q(2)) l2*cos(q(1)+q(2))];
        
    dJ1dq1 = [-l1*cos(q(1)) 0; -l1*sin(q(1)) 0];
    
    dJ1dt = dJ1dq1*q(3);
    
    dJ2dq1 = [-l1*cos(q(1))-l2*cos(q(1)+q(2)) -l2*cos(q(1)+q(2)); -l1*sin(q(1))-l2*sin(q(1)+q(2)) -l2*sin(q(1)+q(2))];
    dJ2dq2 = [-l2*cos(q(1)+q(2)) -l2*cos(q(1)+q(2)); -l2*sin(q(1)+q(2)) -l2*sin(q(1)+q(2))];
    
    dJ2dt = dJ2dq1*q(3) + dJ2dq2*q(4);
    
    ang = q(1);
    R1 = [cos(ang) -sin(ang); sin(ang) cos(ang)];
    ang = (q(1) + q(2));
    R2 = [cos(ang) -sin(ang); sin(ang) cos(ang)];
    
    accL1n = zeros([1 4]);
    
    for i = 1:l1_num
        ddx(2*i-1:2*i) = Rag{i}'*(R1*[interp1(ddxv_l1(:,end),ddxv_l1(:,2*i-1),t); interp1(ddxv_l1(:,end),ddxv_l1(:,2*i),t)].*(1+2*(rand([2 1])-0.5)*inv_snr_l1) - [0 g]');
        J(2*i-1:2*i,:) = J1*l1_ap(i)/l1;
        dJdt(2*i-1:2*i,:) = dJ1dt*l1_ap(i)/l1;
        accL1n(:,3:4) = accL1n(:,3:4) + ddx(2*i-1:2*i)'*l1/norm(l1_ap_vec(1:2,i),2);
    end
    
    accL1n = accL1n/l1_num;
    
    for i = 1:l2_num        
        ddx(2*l1_num+2*i-1:2*l1_num+2*i) = Ra{i+l1_num}'*(R2*[interp1(ddxv_l2(:,end),ddxv_l2(:,2*i-1),t); interp1(ddxv_l2(:,end),ddxv_l2(:,2*i),t)].*(1+2*(rand([2 1])-0.5)*inv_snr_l2) - [0 g]' - accL1n(3:4)') + accL1n(3:4)';
        J(2*l1_num+2*i-1:2*l1_num+2*i,:) = (J2-J1)*l2_ap(i)/l2 + J1;
        dJdt(2*l1_num+2*i-1:2*l1_num+2*i,:) = (dJ2dt-dJ1dt)*l2_ap(i)/l2 + dJ1dt;
    end
%     ddx'
    dq(1:2) = q(3:4);
    dq(3:4) = pinv(J)*(ddx - dJdt*q(3:4));
end

function [Y, N] = obtainTrajFromAccelDataM1(comDataA, accConfigEx, trial_nos, filter)
    if isfield(comDataA, 'Trial')
        [comData, ~, ~] = combineData(comDataA, accConfigEx, trial_nos);
    else
        comData = [];
        
        for i = 1:length(comDataA.A)
            comData = [comData comDataA.A{i}];
        end            
    end

    l1 = accConfigEx.l{1};
    l1_vec = accConfigEx.l_acc{1};
    l2_vec = accConfigEx.l_acc{2};
    
    l1_num = size(l1_vec,2);
    l2_num = size(l2_vec,2);

    il1_vec = zeros([3 l1_num]);
    
    al = zeros([1 l1_num+l2_num]);
    alg = zeros([1 l1_num]);
    
    for i = 1:l1_num        
        il1_vec(:,i) = l1_vec(:,i) - [l1 0 0]';
        
        al(i) = atan2(il1_vec(2,i),abs(il1_vec(1,i)));    
        al(l1_num+i) = atan(l2_vec(2,i)/l2_vec(1,i));
        alg(i) = atan(l1_vec(2,i)/l1_vec(1,i));
        
        Ra{i} = [cos(al(i)) -sin(al(i)); sin(al(i)) cos(al(i))];
        Ra{i+l1_num} = [cos(al(i+l1_num)) -sin(al(i+l1_num)); sin(al(i+l1_num)) cos(al(i+l1_num))];
        Rag{i} = [cos(alg(i)) -sin(alg(i)); sin(alg(i)) cos(alg(i))];
    end

    Y = zeros([size(comData,1) 2]);

    ghat = zeros([size(comData,1) 2]);
    ss11 = zeros([size(comData,1) 2]);

    cnt = l1_num*(l1_num-1)/2;
    ss = comData(:,1:2*l1_num);
    
    A1 = zeros([2*cnt 2]);
    tA1 = zeros([2*cnt 2]);
    B1 = zeros([2*cnt size(comData,1)]);
    tB1 = zeros([2*cnt size(comData,1)]);
    
    c = 1;

    for j = 1:l1_num-1
        for k = j+1:l1_num
            A1(2*c-1:2*c,:) = (norm(il1_vec(:,j),2)*Ra{k} - norm(il1_vec(:,k),2)*Ra{j});
            B1(2*c-1:2*c,:) = (norm(il1_vec(:,j),2)*Ra{k}*ss(:,2*k-1:2*k)' - norm(il1_vec(:,k),2)*Ra{j}*ss(:,2*j-1:2*j)');
            tA1(2*c-1:2*c,:) = (norm(l1_vec(:,k),2)*inv(Rag{j}) - norm(l1_vec(:,j),2)*inv(Rag{k}));
            tB1(2*c-1:2*c,:) =(norm(l1_vec(:,k),2)*inv(Rag{j})*ss(:,2*j-1:2*j)' - norm(l1_vec(:,j),2)*inv(Rag{k})*ss(:,2*k-1:2*k)');
            c = c + 1;
        end
    end
    
    ss11 = ((A1'*A1)\(A1'*B1))';
    ghat = ((tA1'*tA1)\(tA1'*tB1))';

    Y(1:end,1) = -atan2(ghat(1:end,2),ghat(1:end,1)) + pi/2;
    
    ss21 = zeros([size(comData,1) 2]);

    cnt = l2_num*(l2_num-1)/2;
    ss = comData(:,2*l1_num+1:end);
    
    A2 = zeros([2*cnt 2]);
    B2 = zeros([2*cnt size(comData,1)]);
    
    c = 1;

    for j = 1:l2_num-1
        for k = j+1:l2_num
            A2(2*c-1:2*c,:) = (norm(l2_vec(:,k),2)*inv(Ra{l1_num+j}) - norm(l2_vec(:,j),2)*inv(Ra{l1_num+k}));
            B2(2*c-1:2*c,:) = (norm(l2_vec(:,k),2)*inv(Ra{l1_num+j})*ss(:,2*j-1:2*j)' - norm(l2_vec(:,j),2)*inv(Ra{l1_num+k})*ss(:,2*k-1:2*k)');
            c = c + 1;
        end
    end

    ss21 = ((A2'*A2)\(A2'*B2))';

    Y(1:end,2) = atan2(ss11(1:end,2),ss11(1:end,1)) - atan2(ss21(1:end,2),ss21(1:end,1));

%     for i = 2:size(comData,1)
%         if Y(i,1) + 1.75*pi < Y(i-1,1)
%             Y(i,1) = Y(i,1) + 2*pi;
%         elseif Y(i,1) - 1.75*pi > Y(i-1,1)
%             Y(i,1) = Y(i,1) - 2*pi;
%         end
% 
%         if Y(i,2) + 1.75*pi < Y(i-1,2)
%             Y(i,2) = Y(i,2) + 2*pi;
%         elseif Y(i,2) - 1.75*pi > Y(i-1,2)
%             Y(i,2) = Y(i,2) - 2*pi;
%         end
%         
%         if Y(i,1) > 2*pi
%             Y(i,1) = Y(i,1) - 2*pi;
%         end
%         
%         if Y(i,2) > 2*pi
%             Y(i,2) = Y(i,2) - 2*pi;
%         end
%     end

    for i = 2:size(comData,1)
        if Y(i,1) > pi
            Y(i,1) = Y(i,1) - 2*pi;
        elseif Y(i,1) < -pi
            Y(i,1) = Y(i,1) + 2*pi;
        end
        
        if Y(i,2) > pi
            Y(i,2) = Y(i,2) - 2*pi;
        elseif Y(i,2) < -pi
            Y(i,2) = Y(i,2) + 2*pi;
        end
    end

    N = zeros(size(Y));
    
    if filter
        [Yt, N] = filterSignal(Y);
        Y = Yt;
    end
end

function [T, Y] = ode4(fH, tspan, init)
    a = zeros([4 4]);
    a(2,1) = 0.5; 
    a(3,2) = 0.5;
    a(4,3) = 1;
    
    b = [1/6 1/3 1/3 1/6];
    c = [0; 0.5; 0.5; 1];
    
    h = tspan(2)-tspan(1);
    sx = length(init);
    
    T = tspan';
    Y = zeros([length(T) sx]);
    k = zeros([sx 4]);
    
    Y(1,:) = init;
    
    for i = 1:length(T)-1
        k(:,1) = fH(T(i),Y(i,:)');
        k(:,2) = fH(T(i)+c(2)*h,Y(i,:)'+h*a(2,1)*k(1)*ones([sx 1]));
        k(:,3) = fH(T(i)+c(3)*h,Y(i,:)'+h*(a(3,1)*k(1)+a(3,2)*k(2))*ones([sx 1]));
        k(:,4) = fH(T(i)+c(4)*h,Y(i,:)'+h*(a(4,1)*k(1)+a(4,2)*k(2)+a(4,3)*k(3))*ones([sx 1]));
        
        Y(i+1,:) = Y(i,:) + h*(b*k');
    end
end