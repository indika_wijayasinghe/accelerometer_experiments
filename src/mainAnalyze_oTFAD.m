close all
clear all
clc

f = 100;
l1 = 0.5;
l2 = 0.5;
l1_num = 2;
l2_num = 2;

s = '1';

load(['..\data\DATA_' s '.mat'])
eval(['data = double(DATA_' s ');']);

T = linspace(0, size(data,1)/f, size(data,1));

l1_vec = [l1/l1_num:l1/l1_num:l1; 0.2*ones([1 l1_num]); zeros([1 l1_num])];
l2_vec = [l2/l2_num:l2/l2_num:l2; 0.2*ones([1 l2_num]); zeros([1 l2_num])];

accConfig.l1 = l1;
accConfig.l2 = l2;
accConfig.l1_acc = l1_vec;
accConfig.l2_acc = l2_vec;
accData = [data(:,1:2) data(:,4:5) data(:,7:8) data(:,10:11)];

Y = data(:,13:14);
Yr = obtainTrajFromAccelData(accData, accConfig);

figure
plot(T, Y(:,1), 'b', T, Y(:,2), 'r')
hold on
plot(T, Yr(:,1), 'b.', T, Yr(:,2), 'r.')