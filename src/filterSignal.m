function [filSig, noise] = filterSignal(cData)
    d = designfilt('lowpassfir', 'FilterOrder', 100, ...
                 'CutoffFrequency', 3, 'SampleRate', 100);
             
    filSig = zeros(size(cData));
    
    for i = 1:size(cData,2)        
        filSig(:,i) = filtfilt(d,cData(:,i));
    end
    
    noise = cData - filSig;
end