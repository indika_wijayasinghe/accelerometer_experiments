close all

n = 10;

figure; subplot(1,2,1); plot(senData.Trial(1).T,senData.Trial(1).A{n}(:,1:2)); axis([12 19 -12 12])
subplot(1,2,2); plot(senDataS.Trial(1).T,senDataS.Trial(1).A{n}); axis([12 19 -12 12])

for i = 1:10
    [sig,noi] = filterSignal(senData.Trial(1).A{i}(:,1:2));
    s_a(i) = getSNR(sig,noi);
    [sig,noi] = filterSignal(senDataS.Trial(1).A{i}(:,1:2));
    s_s(i) = getSNR(sig,noi);
end

s_a
s_s