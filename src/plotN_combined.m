close all

N = 1000;

n = [2 3 5 6 11 21 26 51 101];
e1 = [0.01675383 0.01665361 0.01478181 0.01398724 0.01102002 0.00840173 0.00760692 0.00567162 0.00429067];
e1_me = [0.00202819 0.00203006 0.00179858 0.00174232 0.00139161 0.00103994 0.00095361 0.00067377 0.00047853]/sqrt(N);
e1_snr = [19.72755287 19.77935295 20.80114898 21.30870296 23.31361942 25.78225969 26.64155985 29.41717448 32.29486510];

figure
hold on
[hAx,hLine1,hLine2] = plotyy(log10(n),e1,log10(n),e1_snr);
hLine1.LineStyle = '--';
hLine1.Marker = 'o';
hLine1.LineWidth = 2;
hLine2.LineStyle = '-.';
hLine2.Marker = 'o';
hLine2.LineWidth = 2;
ylabel(hAx(1),'RMS Error (rad)')
ylabel(hAx(2),'Average SNR (dB)')
names = {'2', '3', '5', '6', '11', '21', '26', '51', '101'};
set(gca,'xtick',log10([2 3 5 6 11 21 26 51 101]),'xticklabel',names)
xlabel('Number of Accelerometers')

% h1 = plot(log10(n),e1,'bo','MarkerSize',6,'LineWidth',2);
% 
% for i = 1:length(e1)
%     me = 1.645*e1_me(i);
%     plot(log10(n(i))*[1 1],[e1(i)-me e1(i)+me],'b-','LineWidth',1)
%     plot(log10(n(i))*[1 1]+0.05*[-1 1],e1(i)+me*[1 1],'b-','LineWidth',1)
%     plot(log10(n(i))*[1 1]+0.05*[-1 1],e1(i)-me*[1 1],'b-','LineWidth',1)
% end
% 
% figure
% hold on
% plot(log10(n),e1_snr,'k-.')
% h1 = plot(log10(n),e1_snr,'bo','MarkerSize',6,'LineWidth',2);
